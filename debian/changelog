untex (1:1.2-10) unstable; urgency=medium

  * QA upload.
  * debian/upstream/metadata: added Bug-Submit field.

 -- David da Silva Polverari <polverari@debian.org>  Sat, 20 Apr 2024 01:08:49 +0000

untex (1:1.2-9) unstable; urgency=medium

  * QA upload.
  * debian/control: bumped Standards-Version to 4.7.0.
  * debian/gbp.conf: created to make git repository comply with DEP-14 layout
    recommendations and enforce the use of pristine-tar.
  * debian/upstream/metadata: created.

 -- David da Silva Polverari <polverari@debian.org>  Tue, 16 Apr 2024 17:57:24 +0000

untex (1:1.2-8) unstable; urgency=medium

  * QA upload.
  * debian/changelog: fixed typo in previous entry.
  * debian/control:
      - Bumped Standards-Version to 4.6.2.
      - Updated Vcs-* fields to use salsa.debian.org.
  * debian/copyright:
      - Fixed upstream license from "Custom" to "GPL-1".
      - Created standalone License stanza for GPL-2+.
      - Updated packaging copyright years.
  * debian/rules: fixed FTCBFS by getting $(CC) from buildtools.mk. Thanks to
    Helmut Grohne <helmut@subdivi.de>. (Closes: #900021)
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to perform a trivial CI test.
  * debian/watch: changed download URI to a secure variant.

 -- David da Silva Polverari <polverari@debian.org>  Sat, 06 Apr 2024 02:24:31 +0000

untex (1:1.2-7) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer (see #920058).
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level from >= 9 to 13.
  * debian/control:
      - Bumped Standards-Version from 3.9.8 to 4.5.0.
      - Set Rules-Requires-Root: no.
  * debian/copyright:
      - Removed some tab characters from the license text.
      - Updated packaging copyright information.
  * debian/rules:
      - Removed "DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed" flag.
      - Removed "DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic" flag.
  * debian/watch:
      - Bumped version from 3 to 4.
      - Removed comment.
      - Updated upstream url.

 -- Fabio dos Santos Mendes <fmendes@protonmail.ch>  Wed, 13 May 2020 18:14:43 -0300

untex (1:1.2-6) unstable; urgency=medium

  * debian/control
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - Update URLs an fix typos.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 20 Oct 2016 17:01:26 +0300

untex (1:1.2-5) unstable; urgency=low

  [ Jari Aalto ]
  * debian/control
    - (Homepage): Update URL. Patch thanks to Stefan Breunig
      <stefan+debianbugs@mathphys.fsk.uni-heidelberg.de>
      (Closes: #689261).
    - (Standards-Version): Update to 3.9.4.
  * debian/copyright
    - (Source): Update URL. Update year.

  [ tony mancill ]
  * include updated d/watch file; thank you to Bart Martens.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 28 Feb 2013 11:12:47 +0200

untex (1:1.2-4) unstable; urgency=low

  * debian/control
    - (Build-Depends): Rm dpkg-dev; not needed with debhelper 9.
    - (Standards-Version): Update to 3.9.3.1.
  * debian/copyright
    - Update to format 1.0.
  * debian/patches
    - (30, 40): New. Fix Gcc compiling.
  * debian/rules
    - Enable all hardening flags.
    - Use DEB_*_MAINT_* variables.

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 24 Mar 2012 01:20:38 -0400

untex (1:1.2-3) unstable; urgency=low

  * debian/compat
    - Update to 9
  * debian/control
    - (Build-Depends): update to debhelper 9, dpkg-dev 1.16.1.
    - (Standards-Version): Update to 3.9.2.
  * debian/copyright
    - Update to DEP5.
  * debian/rules
    - Use hardened CFLAGS.
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 14 Feb 2012 15:58:42 -0500

untex (1:1.2-2) unstable; urgency=low

  * debian/rules
    - (binary-arch): adjust install calls. Patch thanks to
       Antonio Terceiro <terceiro@softwarelivre.org> (Closes: #575409).

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 25 Mar 2010 22:02:40 +0200

untex (1:1.2-1) unstable; urgency=low

  * New maintainer (Closes: #547222).
  * New upstream release; forced to use epoch.
  * Move to packaging format "3.0 (quilt)".
  * debian/clean
    - New file.
  * debian/compat
    - New file.
  * debian/control
    - (Build-Depends): Add debhelper
    - (Description): rewrite long part.
    - (Homepage): New field.
    - (Standards-Version): Update to 3.8.4.
    - (Vcs-*): new fields.
  * debian/copyright
    - Update full layout to follow generic template.
    - (downloaded from): Update upstream URL.
    - (License): update from untex.c
  * debian/patches
    - (Number 10): Convert inline changes to a patch.
    - (Number 20): Document problems in BUGS section of manual page.
      (Closes: #352788).
  * debian/pod2man.mk
    - New file. Convert original Debian untex.1 from previous version
      into debian/untex.1.pod for later reference.
  * debian/source/format
    - New file.
  * debian/substvars
    - Delete debhelper leftover file.
  * debian/untex.install
    - New file.
  * debian/untex.1.pod
    - New manual page.
  * debian/untex.manpages
    - New file.
  * debian/untex.prerm
    - Add DEBHELPER token (lintian).
  * debian/rules
    - Convert to use dh(1).
  * debian/watch
    - New file.
  * Makefile
    - Delete non-upstream file.
  * untex.1
    - Deleted original Debian manual from version 9210-11.
      The current version comes with one.

 -- Jari Aalto <jari.aalto@cante.net>  Wed, 03 Mar 2010 20:25:54 +0200

untex (9210-11) unstable; urgency=low

  * QA upload.
    + Set maintainer to Debian QA Group <packages@qa.debian.org>.
  * Note source location in debian/copyright. (Closes: #469655).
  * Add set -e to prerm.
  * Update FSF address in debian/copyright.
  * Bump Standards Version to 3.8.3.
    + Remove -s from Makefile and strip in rules. (Closes: #438235).

 -- Barry deFreese <bdefreese@debian.org>  Sat, 31 Oct 2009 09:44:16 -0400

untex (9210-10) unstable; urgency=low

  * fix prerm/postinst scripts . (closes: #129308)

 -- Enrique Zanardi <ezanard@debian.org>  Mon, 29 Dec 2003 16:03:16 +0000

untex (9210-9) unstable; urgency=low

  * Install manpages under /usr/share/man . (Closes: Bug#80772)
  * Changed the maintainer address.

 -- Enrique Zanardi <ezanard@debian.org>  Tue, 20 Feb 2001 09:41:33 +0000

untex (9210-8) unstable; urgency=low

  * Only close a file if previously opened.

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Sun, 28 Dec 1997 13:04:50 +0000

untex (9210-7) unstable; urgency=low

  * Corrected permissions in /usr/doc/untex. (Bug#12679).

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Wed, 10 Sep 1997 00:43:39 +0100

untex (9210-6) unstable; urgency=low

  * depends on libc6.

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Wed, 2 Jul 1997 14:03:37 +0100

untex (9210-5) unstable; urgency=low

  * New mantainer.
  * Converted to new packaging format (Closes Bug#4038,Bug#5111).
  * Added manpage.

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Fri, 14 Mar 1997 17:23:57 +0000

untex (9210-4); priority=LOW

  * Added extended description.
  * Added debian.Changelog.
  * Modified debian.rules and debian.README.

 -- Erick Branderhorst <branderhorst@heel.fgg.eur.nl>
