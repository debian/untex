#   Copyright
#
#      Copyright (C) 2009-2010 Jari Aalto
#      Copyright (C) 1997-2003 Enrique Zanardi
#
#   License
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#   Description
#
#       To learn what TOP LEVEL section to use in manual pages,
#       see POSIX/Susv standard and "tility Description Defaults" at
#       http://www.opengroup.org/onlinepubs/009695399/utilities/xcu_chap01.html#tag_01_11
#
#       This is manual page in Perl POD format. Read more at
#       http://perldoc.perl.org/perlpod.html or run command:
#
#           perldoc perlpod | less
#
#       To check the syntax:
#
#           podchecker *.pod
#
#       Create manual page with command:
#
#           pod2man PAGE.N.pod > PAGE.N

=pod

=head1 NAME

untex - Remove LaTeX commands from input

=head1 SYNOPSIS

  untex [options] FILE ...

=head1 DESCRIPTION

Untex removes all LaTeX commands from file1, extracting the contents
to stdout. If FILE is "-", stdin will be used.

=head1 OPTIONS

=over 4

=item B<-a>

Remove argument of commands. Implies option B<-o>.

=item B<-e>

Remove environment names.

=item B<-g>

Replace all "a (etc.) with ibm characters (CP 850) characters
(german.sty). Implies option B<-u>.

=item B<-giso>:

Like B<-uiso> but also replace all "a (etc.) with iso characters
(german.sty).

=item B<-gascii>:

Like B<-uascii> but replace all "a (etc.) with ascii characters
(german.sty).

=item B<-i>

Process \include and \input.

=item B<-m>

Remove all math

=item B<-o>

Remove options to commands

=item B<-u>

Replace all \"a (etc.) with ibm (CP 850) characters.

=item B<-uiso>

Replace all \"a (etc.) with iso characters.

=item B<-uascii>

Replace all \"a (etc.) with ascii characters (ae, ss, ...).

=back

=head1 ENVIRONMENT

None.

=head1 FILES

None.

=head1 SEE ALSO

tex(1)
unrtf(1)

=head1 AUTHORS

Program was written by Michael Staats

This manual page was written by Enrique Zanardi
<ezanardi@molec1.dfis.ull.es> for the Debian GNU system (but may
be used by others). Updated by Jari Aalto <jari.aalto@cante.net>.
Released under license GNU GPL version 2 or (at your option) any
later version. For more information about license, visit
<http://www.gnu.org/copyleft/gpl.html>.

=cut
